import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';


const routes: Routes = [
  {path: 'reserva', component: ReservasListadoComponent},
  {path: 'reserva/:id', component: ReservasDetalleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
