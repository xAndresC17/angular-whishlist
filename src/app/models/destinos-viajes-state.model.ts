import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function intializeDestinosViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

//ACIONES
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destino Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destino Viajes] vote Up',
    VOTE_DOWN = '[Destinos Viajes] vote Down',
    DELETE_DESTINO = '[Destinos Viajes] Delete',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) { }
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) { }
}

export class voteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) { }
}

export class voteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) { }
}

export class DeleteDestinoAction implements Action {
    type = DestinosViajesActionTypes.DELETE_DESTINO;
    constructor(public destino: DestinoViaje) { }
}


export class InitMyDataAction implements Action {
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
  }


export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
    | voteUpAction | voteDownAction | DeleteDestinoAction | InitMyDataAction;


//REDUCER
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
            ...state,
            items: destinos.map((d) => new DestinoViaje(d,''))
            };
        }
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            let d: DestinoViaje = (action as voteUpAction).destino;
            d.voteUp();
            return { ...state };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            let d: DestinoViaje = (action as voteDownAction).destino;
            d.voteDown();
            return { ...state };
        }
        case DestinosViajesActionTypes.DELETE_DESTINO: {
            var econcontrado = state.items.findIndex(x => x == (action as NuevoDestinoAction).destino)

            state.items.splice(econcontrado, 1);

            return { ...state };
        }


    }
    return state
}

//EFECTS
@Injectable({
    providedIn: 'root'
})
export class DestinoViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private actions$: Actions) { }
}
