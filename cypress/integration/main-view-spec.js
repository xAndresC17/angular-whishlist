describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
      cy.visit('http://localhost:4200');
      cy.contains('angular-whishlist');
      cy.get('h1 b').should('contain', 'HOLA es');
    });

    it('Login correcto y ruta protegida', () => {
      cy.visit('http://localhost:4200/login');
      cy.get(':nth-child(1) > .form-control').
      type('user')
      cy.get(':nth-child(2) > .form-control').
      type('password');
      cy.get(':nth-child(1) > .form-control').should('have.value', 'user');
      cy.get(':nth-child(2) > .form-control').should('have.value', 'password');
      cy.get('.btn').click();
      cy.visit('http://localhost:4200/protected');
      cy.get('p').should('contain','protected works!');
    });

    it('Hay un elemento preferido', () => {
      cy.visit('http://localhost:4200/home');
      cy.get('#nombre').type('Barcelona');
      cy.get('#imagenUrl').type('pepe');
      cy.get('.btn').click();
      cy.contains('- PREFERIDO -');
    }); 
    

    it('Aceso a vuelos y validacion de parametros', () => {
      cy.visit('http://localhost:4200/login');
      cy.get(':nth-child(1) > .form-control').
      type('user')
      cy.get(':nth-child(2) > .form-control').
      type('password');
      cy.get('.btn').click();
      cy.visit('http://localhost:4200/vuelos');
      cy.get('[ng-reflect-router-link="./,5"]').click();
      cy.get('app-vuelos-detalle').should('contain','5');
    });

  }); 

